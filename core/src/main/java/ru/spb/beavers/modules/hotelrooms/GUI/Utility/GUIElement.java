package ru.spb.beavers.modules.hotelrooms.GUI.Utility;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;

import ru.spb.beavers.modules.hotelrooms.GUI.SolutionPanel;

public class GUIElement {

    public static void setTextHeader(String header, int oy, int width, int height, JPanel panel)
    {
        JLabel jl = new JLabel("<html><body width=\"" + width + "\" height=\"" + height
                + "\"><center><b><i>" + header + "</i></b></center></body></html>");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(width, height);
        jl.setLocation(panel.getWidth() / 2 - jl.getWidth() / 2, oy);
        panel.add(jl);
    }

    public static void setTextHeader(String header, int oy, int width, int height, LastElCoords last_el_coords,  JPanel panel)
    {
        JLabel jl = new JLabel("<html><body width=\"" + width + "\" height=\"" + height
                + "\"><center><b><i>" + header + "</i></b></center></body></html>");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(width, height);
        jl.setLocation(panel.getWidth() / 2 - jl.getWidth() / 2, last_el_coords.y + last_el_coords.height + oy);
        panel.add(jl);
        last_el_coords.y = jl.getY();
        last_el_coords.height = jl.getHeight();
    }

    public static void setHtmlText(String url_string, int x, int y, int width, int height, JPanel panel)
    {
        JTextPane jtp = new JTextPane();

        try {
        	File dir = new File(SolutionPanel.class.getResource("./../").toURI());
            URI uri = SolutionPanel.class.getResource(url_string).toURI();
            try {
                jtp.setPage(uri.toURL());
            } catch (Exception e) {
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        JScrollPane scroll = new JScrollPane(jtp);
        scroll.setSize(width, height);
        scroll.setLocation(x, y);
        //scroll.setPreferredSize(new Dimension(width, height));
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        panel.add(scroll);
        //jtp.setEditable(false);
        jtp.setBackground(new Color(240,240,240));
    }

    public static void setHtmlText600Px(String url_string, LastElCoords last_el_coords, JPanel panel)
    {
        setHtmlText(url_string, panel.getWidth() / 2 - 300, last_el_coords.y, 600, last_el_coords.height, panel);
    }
}
