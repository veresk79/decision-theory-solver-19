package ru.spb.beavers.modules.strict_ordering;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by laptop on 12.04.2015.
 */
public class CustomModel extends AbstractTableModel {

    class Relation {
        public String first;
        public String second;

        public Relation(String _first, String _second) {
            first = _first;
            second = _second;
        }
    }

    private ArrayList<Relation> data = new ArrayList<Relation>();
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    public CustomModel() {

    }

    public void eraseData() {
        data.clear();
    }

    public void appendRow(String first, String second) {
        Relation tempRelation = new Relation(first, second);
        data.add(tempRelation);
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex == 0)
            return "Предпочтительный";
        if (columnIndex == 1)
            return "Не предпочтительный";

        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (data.isEmpty())
            return null;

        if (columnIndex == 0)
            return data.get(rowIndex).first;

        if (columnIndex == 1)
            return data.get(rowIndex).second;

        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }
}
