package ru.spb.beavers.modules;


import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.SolutionPanel;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.DescriptionPanel;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.InputPanel;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.ExamplePanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.datainp;


//private InputPanel ip;
//private ExamplePanel ep;
//private Node root_node;
public class CoachDecisionOfStrategyGameModule implements ITaskModule {
    private DescriptionPanel dp;
    private SolutionPanel sp;
    private InputPanel ip;
    private ExamplePanel ep;
    private datainp dt=new datainp();
    private datainp dtc=dt;
    private int tg=0;
    JFileChooser fc=new JFileChooser();
    @Override
    public String getTitle(){
        return "Задача 1.4.1: Решение\nтренера о стратегии игры";
    }

    @Override
    public void initDescriptionPanel(JPanel panel){
        dp = new DescriptionPanel(panel);

    }

    @Override
    public void initSolutionPanel(JPanel panel){
        sp = new SolutionPanel(panel);
    }

    @Override
    public void initInputPanel(JPanel panel) {
        /*ip = new InputPanel(panel, root_node);
        if (ep != null) {
            ip.addComponent(new INewSolverHandler() {
                @Override
                public void newSolver(ConflictSolver solver) {
                    ep.setSolver(solver);
                }
            });
        }*/
        ip= new  InputPanel(panel, dt, ep);
//        ip.ubdpanel(ep, ep.getPanel());
        dt=ip.fetData();
    }

    @Override
    public void initExamplePanel(JPanel panel){
        ep = new ExamplePanel(panel, dt);
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException{
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = fc.showSaveDialog(null);
                if(result == JFileChooser.APPROVE_OPTION)
                {
                    try{
                        File file = new File(fc.getSelectedFile()+".cda");
                        FileOutputStream opf = new FileOutputStream(file);
                        ObjectOutputStream oop = new ObjectOutputStream(opf);
                        oop.writeObject(dt);
                        oop.flush();
                        oop.close();
                    }
                    catch(IOException io)
                    {
                       // System.exit(1);
                    }
                }
            }
        };
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException{
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = fc.showOpenDialog(null);
                if(result == JFileChooser.APPROVE_OPTION)
                {
                    try{

                        File file = fc.getSelectedFile();
                        FileInputStream input = new FileInputStream(file);
                        ObjectInputStream inputObject= new ObjectInputStream(input);
                        datainp dd = (datainp) inputObject.readObject();

                        inputObject.close();
                        ip= new  InputPanel(ip.getpanel(), dd, ep);
                       // ep = new ExamplePanel(ep.getPanel(), dd);
                        ip.setdatatable();
                    }
                    catch(IOException io)
                    {
                        // System.exit(1);
                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }

            }
        };
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException{
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ip= new  InputPanel(ip.getpanel(), dtc, ep);
              //  ep = new ExamplePanel(ep.getPanel(), dtc);
                ip.setdatatable();
            }
        };
    }

}
