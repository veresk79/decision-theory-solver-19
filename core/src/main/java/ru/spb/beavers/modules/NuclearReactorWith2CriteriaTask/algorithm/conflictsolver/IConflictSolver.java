package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.algorithm.conflictsolver;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Challenger;

public interface IConflictSolver {
    public Challenger chose (Challenger nodeA, Challenger nodeB);
}