package ru.spb.beavers.modules.hotelrooms.GUI;

import java.awt.Dimension;

import javax.swing.JPanel;

import ru.spb.beavers.modules.hotelrooms.GUI.Utility.GUIElement;
import ru.spb.beavers.modules.hotelrooms.GUI.Utility.LastElCoords;

public class DescriptionPanel
{
    private LastElCoords last_el_coords;

    public DescriptionPanel(JPanel panel)
    {
        last_el_coords = new LastElCoords();

        panel.setLayout(null);
        panel.setSize(750, 1000);
        panel.setPreferredSize(new Dimension(750, 1000));

        // Неформальная постановка задачи
        GUIElement.setTextHeader("Неформальная постановка задачи", 20, 300, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 150);
        GUIElement.setHtmlText600Px("./../html/informal_task_description.html", last_el_coords, panel);

        // Формальная постановка задачи
        GUIElement.setTextHeader("Формальная постановка задачи", 30, 300, 20, last_el_coords, panel);
        last_el_coords.updateValues(10, 250);
        GUIElement.setHtmlText600Px("./../html/formal_task_description.html", last_el_coords, panel);
    }
}