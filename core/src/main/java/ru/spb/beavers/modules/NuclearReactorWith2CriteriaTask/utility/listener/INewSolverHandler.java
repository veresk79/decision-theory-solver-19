package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.algorithm.conflictsolver.ConflictSolver;

public interface INewSolverHandler {
    public void newSolver(ConflictSolver solver);
}
